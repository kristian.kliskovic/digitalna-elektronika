--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:14:25 12/23/2019
-- Design Name:   
-- Module Name:   /home/kristian/Dokumenti/git_KV_DE/kv_de/KV_hehe/input_w.vhd
-- Project Name:  KV_hehe
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: inputs
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY input_w IS
END input_w;
 
ARCHITECTURE behavior OF input_w IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT inputs
    PORT(
			cp : IN  std_logic;
         ulaz : IN  std_logic_vector(6 downto 0);
         broj1 : OUT  std_logic_vector(6 downto 0);
         broj2 : OUT  std_logic_vector(6 downto 0)
        );
    END COMPONENT;
    

   --Inputs
	signal cp : std_logic := '0';
   signal ulaz : std_logic_vector(6 downto 0) := (others => '0');

 	--Outputs
   signal broj1 : std_logic_vector(6 downto 0);
   signal broj2 : std_logic_vector(6 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   --constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: inputs PORT MAP (
          cp => cp,
          ulaz => ulaz,
          broj1 => broj1,
          broj2 => broj2
        );

   -- Clock process definitions
   process
   begin
		cp <= '0';
		wait for 1 ps;
		cp <= '1';
		wait for 1 ps;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	
		ulaz<="1000111";
      
      -- insert stimulus here 
		--wait for <clock>_period*50;
      wait;
   end process;

	--cp<=not cp after 10 ns;

END;
