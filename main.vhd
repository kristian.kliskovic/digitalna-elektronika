----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:17:17 12/23/2019 
-- Design Name: 
-- Module Name:    main - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE; 
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main is
port(
	cp:IN std_logic;
	broj: IN std_logic_vector(6 downto 0);
	smjer: IN std_logic;
	dgts: OUT std_logic_vector(3 downto 0);
	segments: OUT std_logic_vector(6 downto 0);
	dot: OUT std_logic
);
end main;

architecture Behavioral of main is
	signal cp_0:std_logic;
	signal broj1, broj2: std_logic_vector(6 downto 0);
	signal dgtsD, dgtsJ: std_logic_vector(3 downto 0);
	signal f: std_logic;
	begin 
	dot<='1';
	shifter:entity work.six_way port map(cp, smjer, dgtsD, dgtsJ);
	input: entity work.inputs port map(cp, broj, broj1, broj2);
	divider:entity work.freqDivGen generic map(100_000) port map(cp, cp_0);--- bilo je 10_000, za simualciju je 2
	 
	process(cp_0, smjer, broj1, broj2, dgtsJ ,dgtsD)
	begin
		if(cp_0'event and cp_0='1') then
		if(f='0') then
			f<='1';
			if(dgtsD(3)='1') then
				segments<=broj1;
				dgts<="0111";
			elsif(dgtsD(2)='1') then
				segments<=broj1;
				dgts<="1011";
			elsif(dgtsD(1)='1') then
				segments<=broj1;
				dgts<="1101";
			elsif(dgtsD(0)='1') then
				segments<=broj1;
				dgts<="1110";
			else
				dgts<="1111";
				segments<="1111111";
			end if;
		else
			f<='0';
			if(dgtsJ(3)='1') then
				segments<=broj2;
				dgts<="0111";
			elsif(dgtsJ(2)='1') then
				segments<=broj2;
				dgts<="1011";
			elsif(dgtsJ(1)='1') then
				segments<=broj2;
				dgts<="1101";
			elsif(dgtsJ(0)='1') then
				segments<=broj2;
				dgts<="1110";
			else
				dgts<="1111";
				segments<="1111111";
			end if;
		end if;
		end if;
	end process;
end Behavioral;

