--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   19:06:53 12/23/2019
-- Design Name:   
-- Module Name:   /home/kristian/Dokumenti/git_KV_DE/kv_de/KV_hehe/six_way_w.vhd
-- Project Name:  KV_hehe
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: six_way
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY six_way_w IS
END six_way_w;
 
ARCHITECTURE behavior OF six_way_w IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT six_way
    PORT(
			desetice: IN std_logic_vector(6 downto 0);
         cp : IN  std_logic;
         smjer : IN  std_logic;
         dgt : buffer  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
	signal desetice : std_logic_vector(6 downto 0) := "0000000";
   signal cp : std_logic := '0';
   signal smjer : std_logic := '0';

 	--Outputs
   signal dgt : std_logic_vector(3 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   --constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: six_way PORT MAP (
			 desetice => desetice,
          cp => cp,
          smjer => smjer,
          dgt => dgt
        );

   -- Clock process definitions
   process
   begin
		cp <= '0';
		wait for 10 ps;
		cp <= '1';
		wait for 10 ps;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
		desetice<="1111111";
      -- hold reset state for 100 ns.
      wait for 1000 ms;	

      --wait for <clock>_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
