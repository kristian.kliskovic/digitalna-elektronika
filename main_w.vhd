-- TestBench Template 

  LIBRARY ieee;
	use IEEE.STD_LOGIC_1164.ALL;
	use IEEE.STD_LOGIC_arith;
	use IEEE.STD_LOGIC_signed;
	use ieee.numeric_std.ALL; 
	LIBRARY UNISIM;
	use UNISIM.Vcomponents.ALL;

  ENTITY testbench IS
  END testbench;

  ARCHITECTURE behavior OF testbench IS 

  -- Component Declaration
          COMPONENT main
          PORT(
                  cp : IN std_logic;
                  broj : IN std_logic_vector(6 downto 0);
						smjer: IN std_logic;
                  dgts : OUT std_logic_vector(3 downto 0);
						segments: OUT std_logic_vector(6 downto 0);
						dot: OUT std_logic;
						where: buffer std_logic_vector(3 downto 0)
          );
          END COMPONENT;
			 
			 signal cp			: std_logic;
			 signal broj		: std_logic_vector(6 downto 0);
			 signal smjer		: std_logic;
			 signal dgts		: std_logic_vector(3 downto 0);
			 signal segments	: std_logic_vector(6 downto 0);
			 signal dot			: std_logic;
			 signal where		: std_logic_vector(3 downto 0);

  BEGIN

  -- Component Instantiation
          uut: main PORT MAP(
                  broj => broj,
						cp => cp,
						smjer => smjer,
                  dgts => dgts,
						segments => segments,
						dot => dot,
						where => where
          );
 

  --  Test Bench Statements
  
		process
		begin
			cp <= '1';
			wait for 5 ps;
			cp <= '0';
			wait for 5 ps;
		end process;
		
		
     tb : PROCESS
     BEGIN
			smjer<='0';
			broj<="0001001";

        wait for 100000 ms; -- wait until global set/reset completes

        -- Add user defined stimulus here

        wait; -- will wait forever
     END PROCESS tb;
  --  End Test Bench 

  END;
