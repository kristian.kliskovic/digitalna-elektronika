library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity freqDivGen is
generic(nfclk:natural:=40000000);
port(
	clk: std_logic;
	clk_o: buffer std_logic
);
end freqDivGen;

architecture Behavioral of freqDivGen is
	begin
	process(clk)
		variable t: integer range 0 to nfclk/2:=0;
		begin
		if(clk'event 	and clk='1') then
			t:=t+1;
			if(t>=nfclk/2) then
				clk_o<=not clk_o;
				t:=0;
			end if;
		end if;
	end process;
end Behavioral;



