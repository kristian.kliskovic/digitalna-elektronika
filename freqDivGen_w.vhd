--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   02:42:26 12/25/2019
-- Design Name:   
-- Module Name:   /home/kristian/Dokumenti/KV_DE/Digitalna elektronika/freqDivGen_w.vhd
-- Project Name:  OPEN_ME
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: freqDivGen
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY freqDivGen_w IS
END freqDivGen_w;
 
ARCHITECTURE behavior OF freqDivGen_w IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT freqDivGen
    PORT(
         clk : IN  std_logic;
         clk_o : buffer  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';

 	--Outputs
   signal clk_o : std_logic:='0';

   -- Clock period definitions
   --constant clk_period : time := 10 ns;
   --constant clk_o_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: freqDivGen PORT MAP (
          clk => clk,
          clk_o => clk_o
        );

   -- Clock process definitions
   process
   begin
		clk <= '0';
		wait for 1 ns;
		clk <= '1';
		wait for 3 ns;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      wait for 100 ns;	
      wait;
   end process;

END;
