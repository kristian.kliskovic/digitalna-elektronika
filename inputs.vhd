----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:14:21 12/23/2019 
-- Design Name: 
-- Module Name:    inputs - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith;
use IEEE.STD_LOGIC_signed;
use ieee.numeric_std.ALL; 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity inputs is
	port(
		cp: IN std_logic;
		ulaz: IN std_logic_vector(6 downto 0);
		broj1: OUT std_logic_vector(6 downto 0);
		broj2: OUT std_logic_vector(6 downto 0)
	);
end inputs;


	
architecture Behavioral of inputs is
	shared variable temp, temp1, temp2: integer;
	begin
	process(cp) 

		begin
		if(cp='1' and cp'event) then
			temp := to_integer(unsigned(ulaz));
			if(temp<100) then
				temp2:=temp mod 10;
				temp1:=(temp-temp2) /10;
			else
				temp2:=10;
				temp1:=10;
			end if;
		
			if(temp1=0) then
				broj1<="1111111";--znamenka desetica neka je ugasena
			end if;
			if(temp1=1) then
				broj1<="1001111";
			end if;
			if(temp1=2) then
				broj1<="0010010";
			end if;
			if(temp1=3) then
				broj1<="0000110";
			end if;
			if(temp1=4) then
				broj1<="1001100";
			end if;
			if(temp1=5) then
				broj1<="0100100";
			end if;
			if(temp1=6) then
				broj1<="0100000";
			end if;
			if(temp1=7) then
				broj1<="0001111";
			end if;
			if(temp1=8) then
				broj1<="0000000";
			end if;
			if(temp1=9) then
				broj1<="0000100";
			end if;
			if(temp1=10) then 
				broj1<="1111111";
			end if;
		
			if(temp2=0) then
				broj2<="0000001";
			end if;
			if(temp2=1) then
				broj2<="1001111";
			end if;
			if(temp2=2) then
				broj2<="0010010";
			end if;
			if(temp2=3) then
				broj2<="0000110";
			end if;
			if(temp2=4) then
				broj2<="1001100";
			end if;
			if(temp2=5) then
				broj2<="0100100";
			end if;
			if(temp2=6) then
				broj2<="0100000";
			end if;
			if(temp2=7) then
				broj2<="0001111";
			end if;
			if(temp2=8) then
				broj2<="0000000";
			end if;
			if(temp2=9) then
				broj2<="0000100";
			end if;
			if(temp2=10) then
				broj2<="1111111";
			end if;
		end if;
	end process;

end Behavioral;