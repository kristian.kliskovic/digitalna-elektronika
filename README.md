Buduci da nigdje nisam pisao komentare u kodu :(

inputs.vhd fajl:

Uzima inpute u obliku jednog 7-bitnog vektora (koristim 7 bitova jer mi to pokriva brojece od 0-99, iako je dogovoreno da se micu brojevi od 0-15, no promjenom jednog broja u kodu mijenjam opseg) i vraca dva sedam bitna broja koja odreduju izgled brojke desetica i jedinica u sedam-segmentnom obliku.

six_way.vhd fajl:

Svakih X sekundi mijenja stanje 4-bitnog vektora "where", moguca su 6 stanja (ZZZZ, ZZZ0,ZZ01, Z01Z,01ZZ,1ZZZ), osim ako je znamenka desetica (koja dolazi iz inputs fajla preko maina) jednaka "1111111", odnosno da je znamenka desetica jednaka nuli, onda se preskace stanje ZZZZ, da bi izgledalo "prirodnije" kada je postavljen broj manji od deset, inace bi bilo da duplo duze bude zaslon ugasen nego sto inace bude svako drugo stanje.

main.vhd fajl:

svaki drugi put (kada je ff='1') kada se dogodi rastuci brid cp_0 pretrazi 4-bitni vektor "where" i ako vidi '0' negdje u tom vektora onda nek stavi nulu na to mjesto u izlaznom vektoru koji ide na common spojeve segmentata (d1,d2,d3,d4) a neka vec spreman izgled znamenke desetica ode na izlazni vektor koji kontrolira zajednicke spojeve pojedinih segmenata (A,B,C...G),

svaki 2n+1 put kada se dogodi rastuci brid cp_0 (kada je ff='0') program trazi '1' u vektoru "where" i tamo stavi znamenku jedinica ,odnosno postavi segmente da pokazuje znamenku jedinica, a upali samo taj digit na kojem je mjestu jedinica u vektoru "where".

Potrebno je podesiti frekvencije, trenutno su namjestene za simulaciju.