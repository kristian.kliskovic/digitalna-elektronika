----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    00:37:39 12/23/2019 
-- Design Name: 
-- Module Name:    six_way - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
entity six_way is
port(
	--desetice: IN std_logic_vector(6 downto 0);
	cp: IN std_logic;
	smjer: IN std_logic;
	dgtD: buffer std_logic_vector(3 downto 0);
	dgtJ: out std_logic_vector(3 downto 0)
	);
end six_way;

architecture Behavioral of six_way is
	type stanje is (s1,s2,s3,s4,s5,s6);
	signal tren, bud: stanje;
	signal cp_q: std_logic;
	 
	begin
	s:entity work.freqDivGen generic map(50_000_000) port map(cp, cp_q);-- bilo je 50_000_000
	process(cp_q)
		begin
		if(cp_q'event and cp_q='1') then
			tren<=bud;
		end if;
	end process; 
	
	
	bud<= s1 when tren=s6 and smjer='0' else
			s2 when tren=s1 and smjer='0' else
			s3 when tren=s2 and smjer='0' else
			s4 when tren=s3 and smjer='0' else
			s5 when tren=s4 and smjer='0' else
			s6 when tren=s5 and smjer='0' else
			
			s1 when tren=s2 and smjer='1' else
			s6 when tren=s1 and smjer='1' else
			s5 when tren=s6 and smjer='1' else
			s4 when tren=s5 and smjer='1' else
			s3 when tren=s4 and smjer='1' else
			s2 ;
			
	dgtD<="0001" when tren=s2 else
			"0010" when tren=s3 else
			"0100" when tren=s4 else
			"1000" when tren=s5 else
			"0000";
	dgtJ<="0001" when tren=s3 else
			"0010" when tren=s4 else
			"0100" when tren=s5 else
			"1000" when tren=s6 else
			"0000";
	
	
end Behavioral;


